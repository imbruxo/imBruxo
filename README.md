# Call Me Bruxo 🧙🏽‍♂️

## Junior DevOps Engineer | Python, Bash, Rust ,C Enthusiast  👨🏻‍💻

<p align="center">
  <br><img src="https://github.com/voidbruxo/workers-cloudflare/blob/main/Other/pics/snake.svg" width="1480px">
</p>


## Status 💡

<p align="Right"><a href="https://github.com/anuraghazra/github-readme-stats">
  <img align="Right" src="https://github-readme-stats.vercel.app/api?username=voidbruxo&show_icons=true&theme=tokyonight" />
</a></p>

[![GitHub Streak](https://github-readme-streak-stats.herokuapp.com?user=voidbruxo&theme=github-dark-dimmed&hide_border=true&border_radius=1.8&date_format=j%20M%5B%20Y%5D&exclude_days=Sun%2CMon%2CTue%2CWed%2CThu%2CFri%2CSat&card_width=500)](https://git.io/streak-stats)

[![](https://visitcount.itsvg.in/api?id=voidbruxo&label=Profile%20Visit&pretty=true)](https://visitcount.itsvg.in)

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Contact Information 🤙🏻

[![icons8-GitHub-64](https://img.icons8.com/arcade/64/github.png)](https://github.com/voidbruxo)
[![icons8-Telegram-64](https://img.icons8.com/arcade/64/telegram-app.png)](https://t.me/imbruxo)
[![icons8-gmail-64](https://img.icons8.com/?size=60&id=JvavVaAzDz8e&format=png)](mailto:braxbot@protonmail.com)
[![icons8-LinkedIn-64](https://img.icons8.com/arcade/64/linkedin-circled.png)](https://au.linkedin.com/in/voidbruxo)

### Let's connect and collaborate to elevate our shared expertise in the world of technology! ✅
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
